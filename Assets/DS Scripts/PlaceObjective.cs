﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaceObjective : MonoBehaviour {

    public GameObject[] items;

    private Encounter encounter;
    private HashSet<GameObject> inside, remaining, total;

    private void Awake() {
        Debug.Log("Tool event Awake");

        inside = new HashSet<GameObject>();
        remaining = new HashSet<GameObject>();
        total = new HashSet<GameObject>();

        foreach (GameObject g in items) {
            total.Add(g);
            remaining.Add(g);
        }
        encounter = GetComponentInParent<Encounter>();
    }

    private void OnTriggerEnter(Collider other) {
        if (!isValidObject(other.gameObject))
            return;

        Debug.Log("Object placed in target: " + other.name);

        inside.Add(other.gameObject);
        remaining.Remove(other.gameObject);
        if (remaining.Count == 0) {
            encounter.Complete();
        }
    }

    private void OnTriggerExit(Collider other) {
        inside.Remove(other.gameObject);
        if (total.Contains(other.gameObject)) {
            remaining.Add(other.gameObject);
        }
    }

    /// <summary>
    /// Determine whether the triggering object is one of the tracked objects for this exercise.
    /// </summary>
    /// <param name="_go"></param>
    /// <returns></returns>
    private bool isValidObject(GameObject _go) {
        foreach(GameObject go in items) {
            if (go == _go)
                return true;
        }
        return false;
    }
}
