﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EncounterTest : MonoBehaviour
{
    public Encounter encounter; 

    public void CompleteEncounter()
    {
        encounter.Complete();
    }
}
