﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MosaicVR;

public class PointerTarget : VRCanvasButton {
    public Encounter encounter;
    public Image image;
    public Color normalColor, highlightColor;
    public VRCanvasPointer pointer;

    //private void OnEnable() {
    //    pointer.Toggle(true);
    //}

    //private void OnDisable() {
    //    pointer.Toggle(false);
    //}

    public override void HoverEnter() {
        SetColor(true);
    }

    public override void HoverExit() {
        SetColor(false);
    }

    public override void SelectUp() {
        if (encounter)
            encounter.Complete();
        SetColor(false);
    }

    private void SetColor(bool highlighted) {
        image.color = highlighted ? highlightColor : normalColor;
    }
}
