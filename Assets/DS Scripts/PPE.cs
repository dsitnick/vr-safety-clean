﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NewtonVR;

public class PPE : MonoBehaviour {

    public Mannequin mannequin;

    private HashSet<GameObject> itemsLeft;
    private Animator animator;
    private NVRInteractableItem[] items;

    private bool locked;
    private Encounter encounter;

    public string IncorrectPrompt;

    void Awake() {
        animator = GetComponent<Animator>();
        items = GetComponentsInChildren<NVRInteractableItem>(true);
        foreach (Wearable w in GetComponentsInChildren<Wearable>(true)) {
            w.ppe = this;
        }
        encounter = GetComponent<Encounter>();
    }

    void Start () {
        itemsLeft = new HashSet<GameObject>();
        foreach (Wearable w in GetComponentsInChildren<Wearable>(true)) {
            if (w.prop != null) {
                itemsLeft.Add(w.prop);
            }
        }
        locked = false;
	}

    public void EquipItem(GameObject item) {
        if (locked)
            return;

        item.SetActive(true);
        itemsLeft.Remove(item);
        if (itemsLeft.Count == 0) {
            encounter.Complete();
            locked = true;
        }

        //locked = true; //Permantently locks PPE (Why? - DMW)
    }

    public void WrongItem() {
        if (locked)
            return;

        if (IncorrectPrompt != "")
            MessageCanvas.Message(IncorrectPrompt);
        animator.SetTrigger("Wrong");
    }

    public void Lock() { locked = true; }
    public void Unlock() { locked = false; }
}
