﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Encounter : MonoBehaviour {

    public string contextID, taskID;
    public PlayerMode.Mode mode;
    private PlayerMode playerMode;

    // Indicates whether the Encounter needs to be teleported to and whether the user is automatically placed there
    public bool TeleportRequired = true;
    public bool AutoTeleport = false;

    // Indicates whether the instructor needs to expose the waypoint manually
    public bool AutoWaypoint = false;

    // Populate with GameObjects that should be hidden after the encounter is complete.
    // (UI pages, world-space indicators, etc.)
    public GameObject[] TemporaryObjects;

    // If given a value, this will be sent to the MessageCanvas to guide the user toward completion of this encounter.
    public string BeginPrompt;
    public string TeleportPrompt;
    public string CompletePrompt;

    /// <summary>
    /// Find the Encounter in the scene with the given TaskID, if available.
    /// </summary>
    /// <param name="_taskID">TaskID of the Encounter/EventNode</param>
    /// <returns>Encounter component if available, NULL if not.</returns>
    public static Encounter FindEncounterWithID(string _taskID)
    {
        Encounter[] encounters = Resources.FindObjectsOfTypeAll<Encounter>();
        foreach(Encounter e in encounters)
        {
            if (e.taskID == _taskID)
            {
                return e;
            }
        }
        return null;
    }

    protected virtual void Awake() {
        playerMode = FindObjectOfType<PlayerMode>();
    }

    public void Begin() {
        if (BeginPrompt != "") {
            MessageCanvas.Message(BeginPrompt);
        }

        if (!TeleportRequired) {
            BeaconWaypoint wp = GetComponentInChildren<BeaconWaypoint>();
            if (wp) {
                wp.Teleport();
            }
        }
        else if (AutoTeleport) {
            BeaconWaypoint wp = GetComponentInChildren<BeaconWaypoint>();
            if (wp) {
                if (playerMode) {
                    playerMode.transform.position = wp.transform.position;
                    playerMode.transform.rotation = wp.transform.rotation;
                }

                wp.Teleport();
            }
        }
    }

    public void Teleport() {
        if (playerMode)
            playerMode.SetMode(mode);

        if (TeleportPrompt != "") {
            MessageCanvas.Message(TeleportPrompt);
        }
    }

    public void Complete() {
        EventManager.Complete(contextID, "complete");

        foreach(GameObject go in TemporaryObjects)
        {
            go.SetActive(false);
        }

        if (CompletePrompt != "") {
            MessageCanvas.Message(CompletePrompt);
        }
    }
}
