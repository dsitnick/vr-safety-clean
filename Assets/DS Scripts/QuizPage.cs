﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MosaicVR;
using UnityEngine.UI;

public class QuizPage : VRFlatPage {

    private Animator animator;
    private QuizButton[] buttons;
    public Encounter encounter;

    private int numAnswersNeeded;
    private List<QuizButton> userAnswers;

    protected override void Awake() {
        base.Awake();
        buttons = GetComponentsInChildren<QuizButton>();
        animator = GetComponent<Animator>();
    }

    private void Start() {
        numAnswersNeeded = 0;
        userAnswers = new List<QuizButton>();
        foreach (QuizButton b in buttons) {
            b.Initialize(this);
            if (b.correct) {
                numAnswersNeeded++;
            }
        }

    }

    public void Begin() {
        Lock(false);
    }

    public void Answer(QuizButton button) {
        if (userAnswers.Contains(button)) {
            userAnswers.Remove(button);
        } else {
            userAnswers.Add(button);
        }
    }

    public void AnimLock() {
        Lock(true);
    }
    public void AnimUnlock() {
        Lock(false);
    }

    private void Lock(bool locked) {
        foreach (QuizButton b in buttons) { b.Lock(locked); }
    }

    public void CheckAnswers() {
        Debug.Log("Check answers");

        Debug.Log(userAnswers.Count + ", " + numAnswersNeeded);
        bool correct = userAnswers.Count == numAnswersNeeded;

        foreach(QuizButton b in userAnswers) {
            Debug.Log(b.correct);
            correct = correct && b.correct;
        }

        if (correct) {
            animator.SetTrigger("Correct");
            foreach (QuizButton b in buttons) { b.Mark(); }
            encounter.Complete();
        } else {
            animator.SetTrigger("Incorrect");
        }
    }
}
