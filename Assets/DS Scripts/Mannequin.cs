﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NewtonVR;

public class Mannequin : MonoBehaviour { 

    void OnTriggerEnter (Collider c) {
        Wearable i = c.GetComponent<Wearable> ();
        if (i == null)
            return;

        i.inMannequin = true;
    }

    void OnTriggerExit (Collider c) {
        Wearable i = c.GetComponent<Wearable> ();
        if (i == null)
            return;

        i.inMannequin = false;
    }
}
