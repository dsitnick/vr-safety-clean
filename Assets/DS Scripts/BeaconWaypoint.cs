﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MosaicVR;

public class BeaconWaypoint : MinimapObject {

    public MeshRenderer waypointRenderer;
    public Color waypointColor;
    public GameObject[] activated;
    private Encounter encounter;

    private void Awake() {
        Material[] m = waypointRenderer.materials;
        for (int i = 0; i < 4; i++) {
            if (i == 2)
                continue; //Skip base

            m[i].SetColor("_Color", waypointColor);
            m[i].SetColor("_BackColor", waypointColor);
        }
        encounter = GetComponentInParent<Encounter>();
    }

    public override void Enable() {
        base.Enable();
        waypointRenderer.gameObject.SetActive(true);
    }

    public override void Disable() {
        base.Disable();
        waypointRenderer.gameObject.SetActive(false);
        foreach (GameObject g in activated){
            g.SetActive(false);
        }
    }

    public override void Teleport() {
        base.Teleport();
        waypointRenderer.gameObject.SetActive(false);
        foreach (GameObject g in activated) {
            g.SetActive(true);
        }

        if (encounter) {
            encounter.Teleport();
        } else {
            Debug.LogWarning("No encounter found for waypoint under " + transform.parent.name);
        }

    }
}
