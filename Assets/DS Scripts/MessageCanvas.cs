﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MessageCanvas : MonoBehaviour {

    public float distance, smoothing;

    public Text text;
    public Transform head, root;
    private Animator animator;
    public Animator Animator { get { return animator; } }

    private static MessageCanvas INSTANCE;

    private void Awake() {
        animator = GetComponent<Animator>();
        INSTANCE = this;
    }

    public static void Message(string message) {
        INSTANCE.text.text = message;
        INSTANCE.Animator.SetTrigger("Message");
    }

    private void Update() {
        Vector3 p = root.position;
        Vector3 v = head.position + head.forward * distance;
        root.position = Vector3.Lerp(p, v, smoothing);
        root.rotation = Quaternion.LookRotation(head.forward, head.up);
    }
}
