﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MosaicVR;

public class QuizWaypoint : BeaconWaypoint {

    public VRCanvasPointer pointer;

    public override void Disable() {
        base.Disable();
      //  pointer.Toggle(false);
    }

    public override void Teleport() {
        base.Teleport();
       // pointer.Toggle(true);
    }
}
