﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NewtonVR;

public class Wearable : MonoBehaviour {

    public bool inMannequin;
    public GameObject prop; //Leave null if wrong answer
    public PPE ppe;

    private NVRInteractableItem item;

    private void Awake() {
        item = GetComponent<NVRInteractableItem>();
        item.OnEndInteraction.AddListener(delegate {
            Release();
        });
    }

    public void Release() {
        item.transform.localPosition = Vector3.zero;
        item.transform.localRotation = Quaternion.identity;

        if (inMannequin) {
            if (prop == null) {
                ppe.WrongItem();
            } else {
                ppe.EquipItem(prop);
                //gameObject.SetActive(false); //Disable forever
                item.enabled = false;
            }
        }
    }

    public void Hovering() {
        //Debug.Log("Hovering");
    }

    public void Reset() {
        
    }
}
