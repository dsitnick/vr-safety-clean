﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MosaicVR;

public class QuizButton : VRCanvasButton {

    public bool correct;
    public Color color, highlightColor, disabledColor, correctColor;

    private Image image;
    private Text[] texts;

    private bool locked, hovered, marked;
    private QuizPage quizPage;

    private void Awake() {
        image = GetComponent<Image>();
        texts = GetComponentsInChildren<Text>();
    }

    public void Initialize(QuizPage quizPage) {
        this.quizPage = quizPage;
        marked = false;
        SetColor(false);
    }

    public override void HoverEnter() {
        if (marked)
            return;

        hovered = true;
        if (locked)
            return;

        SetColor(true);
    }

    public override void HoverExit() {
        hovered = false;
        if (locked || marked)
            return;

        SetColor(false);
    }

    public override void SelectDown() {
        marked = !marked;
        quizPage.Answer(this);
    }

    public override void SelectUp() {
        if (locked || marked)
            return;
    }

    public void Lock(bool locked) {
        this.locked = locked;
        SetColor(hovered);
    }

    public void Mark() {
        marked = true;
        SetColor(false);
        if (correct)
            foreach (Text t in texts)
                t.color = correctColor;
    }

    private void SetColor(bool highlighted) {
        image.color = highlighted ? highlightColor : color;
        foreach (Text t in texts)
            t.color = (marked && !highlighted) ? disabledColor : (highlighted ? color : highlightColor);
    }
}
