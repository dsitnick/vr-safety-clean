﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NewtonVR;

public class SnapObjective : MonoBehaviour {

    public NVRInteractableItem item;

    public Material ghostMaterial;

    private GameObject ghost;
    private Encounter encounter;
    Vector3 originalPos;
    Quaternion originalRot;

    private void Awake() {
        encounter = GetComponentInParent<Encounter>();
        GameObject g = Instantiate(item.gameObject, transform);
        g.transform.localPosition = Vector3.zero;
        g.transform.localRotation = Quaternion.identity;

        List<MeshRenderer> renderers = new List<MeshRenderer>();
        foreach (MeshRenderer r in g.GetComponents<MeshRenderer>()) {
            renderers.Add(r);
        }
        foreach (MeshRenderer r in g.GetComponentsInChildren<MeshRenderer>()) {
            renderers.Add(r);
        }
        foreach (MeshRenderer r in renderers) {
            r.material = ghostMaterial;
        }
        foreach (Collider c in g.GetComponents<Collider>()) {
            c.enabled = false;
        }
        foreach (Collider c in g.GetComponentsInChildren<Collider>()) {
            c.enabled = false;
        }
        Destroy(g.GetComponent<Rigidbody>());
        ghost = g;

        item.OnEndInteraction.AddListener(delegate {
            SnapItem();
        });
        inside = false;
        ghost.SetActive(true);

        originalPos = item.transform.position;
        originalRot = item.transform.rotation;
    }

    private void SnapItem() {
        if (inside) {
            ghost.SetActive(false);
            item.transform.position = ghost.transform.position;
            item.transform.rotation = ghost.transform.rotation;
            item.enabled = false;
            encounter.Complete();
        } else {
            item.transform.position = originalPos;
            item.transform.rotation = originalRot;
        }
    }

    bool inside;
    private void OnTriggerEnter(Collider other) {
        if (other.gameObject == item.gameObject) {
            inside = true;
        }
    }

    private void OnTriggerExit(Collider other) {
        if (other.gameObject == item.gameObject) {
            inside = false;
        }
    }

}
