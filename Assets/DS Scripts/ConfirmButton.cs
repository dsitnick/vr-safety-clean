﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MosaicVR;

public class ConfirmButton : VRCanvasButton {
    private Image m_Image;
    public QuizPage Quiz;

    public Color HighlightColor;
    public Color NormalColor;

    private void Start() {
        m_Image = GetComponent<Image>();
    }

    public override void HoverEnter() {
        SetColor(true);
    }

    public override void HoverExit() {
        SetColor(false);
    }

    public override void SelectUp() {
        SetColor(false);
        Quiz.CheckAnswers();
    }

    private void SetColor(bool highlighted) {
        if (m_Image) {
            m_Image.color = highlighted ? HighlightColor : NormalColor;
        }
    }
}
