﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContextSwitch : MonoBehaviour {
    public GameObject[] CloseContexts;
    public GameObject[] OpenContexts;

	// Use this for initialization
	void OnEnable () {
		foreach(GameObject go in CloseContexts) {
            go.SetActive(false);
        }
        foreach(GameObject go in OpenContexts) {
            go.SetActive(true);
        }

        Encounter e = GetComponentInParent<Encounter>();
        if (e) {
            e.Complete();
        }
	}
}
