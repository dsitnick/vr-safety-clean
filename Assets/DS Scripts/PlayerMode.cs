﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NewtonVR;
using MosaicVR;

public class PlayerMode : MonoBehaviour {

    //Roaming - Free form, teleporting enabled, interaction enabled, pointer disabled: For roaming between encounters
    //Interactive - No teleporting, no pointer, but interaction still enabled: For PPE and action encounters
    //Pointer - No teleporting, no interaction, but pointer is enabled: For quiz and pointer encounters
    public enum Mode { Roaming, Interactive, Pointer };

    public VRCanvasPointer pointer;
    public TeleportVive teleport;
    public NVRHand leftHand, rightHand;
    public Mode mode;

    private void Start() {
        SetMode(mode);
    }

    public void SetMode(Mode mode) {
        this.mode = mode;
        leftHand.Enabled = rightHand.Enabled = (mode == Mode.Interactive || mode == Mode.Roaming);

        pointer.Toggle(mode == Mode.Pointer);

        teleport.Enabled = mode == Mode.Roaming;
    }
}
