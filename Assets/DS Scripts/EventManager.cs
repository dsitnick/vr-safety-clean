﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MosaicVR;
using NewtonVR;

public class EventManager : MonoBehaviour {
    [System.Serializable]
    public struct EventContextDefinition
    {
        public string id;
        public EventNodeDefinition[] eventNodes;
    }

    [System.Serializable]
    public struct EventNodeDefinition
    {
        public string id;
        public string[] parameters;
        public string next_id;
    }
    public EventContextDefinition[] SerializedEventContexts;
    private HashSet<EventContext> m_EventContexts;

    private NVRPlayer player;
    private PlayerMode mode;
    public ParabolicPointer pointer;

    private void Awake() {
        player = FindObjectOfType<NVRPlayer>();
        if (player)
            mode = player.GetComponent<PlayerMode>();

        initializeEventContexts();

        ////Tutorial Context
        //tutorial = new EventContext("tutorial");
        ////Add nodes
        //tutorial.Add("pointer", "complete");
        //tutorial.Add("teleport", "complete");
        //tutorial.Add("ppe", "complete");
        //tutorial.Add("tool", "complete");
        //tutorial.Add("quiz", "complete");

        ////Connect linear sequence of tasks
        //tutorial.Connect("pointer", "teleport");
        //tutorial.Connect("teleport", "ppe");
        //tutorial.Connect("ppe", "tool");
        //tutorial.Connect("tool", "quiz");

        //tutorial.CompletionEvent("pointer").AddListener(delegate {
        //    mode.SetMode(PlayerMode.Mode.Roaming);
        //});
        //tutorial.CompletionEvent("teleport").AddListener(delegate {
        //    mode.SetMode(PlayerMode.Mode.Roaming);
        //});
        //tutorial.CompletionEvent("ppe").AddListener(delegate {
        //    mode.SetMode(PlayerMode.Mode.Roaming);
        //});
        //tutorial.CompletionEvent("tool").AddListener(delegate {
        //    mode.SetMode(PlayerMode.Mode.Roaming);
        //});

        //tutorial.CompletionEvent("quiz").AddListener(delegate {
        //    mode.SetMode(PlayerMode.Mode.Roaming);

        //    //Called when tutorial is complete
        //});
    }

    private void initializeEventContexts()
    {
        m_EventContexts = new HashSet<EventContext>();

        foreach(EventContextDefinition ecd in SerializedEventContexts)
        {
            EventContext tempContext = new EventContext(ecd.id);

            // Add all EventNodes with their parameters and completion events
            foreach (EventNodeDefinition end in ecd.eventNodes)
            {
                tempContext.Add(end.id, end.parameters);

                tempContext.CompletionEvent(end.id).AddListener(delegate {
                    // Return the PlayerMode to roaming (default)
                    if (mode)
                        mode.SetMode(PlayerMode.Mode.Roaming);

                    //Encounter e = Encounter.FindEncounterWithID(end.id);                   
                    //if (e) {
                    //    // If one exists, Disable the Encounter and all of its children
                    //    e.gameObject.SetActive(false);
                    //}
                });

                tempContext.StartEvent(end.id).AddListener(delegate {
                    Encounter e = Encounter.FindEncounterWithID(end.id);
                    if (e) {
                        // Ensure that the Encounter is enabled.
                        e.gameObject.SetActive(true);
                        Debug.Log("Activated " + e.name);

                        // If the new encounter is set to automatically show its waypoint, do so without instructor input
                        if (e.AutoWaypoint) {
                            Debug.Log("Setting waypoint for " + e.name);
                            BeaconWaypoint wp = e.GetComponentInChildren<BeaconWaypoint>();
                            if (wp) {
                                Debug.Log(wp);
                                wp.Enable();
                                Debug.Log(wp);

                                Debug.Log("Setting waypoint for " + e.name);
                                Minimap.SetWaypoint(wp);
                                if (pointer)
                                    pointer.SetSnap(wp);
                            }
                        }

                        // Start the Encounter
                        e.Begin();
                    }
                        
                });
            }

            // Link all EventNodes together (requires that all be instantiated)
            foreach (EventNodeDefinition end in ecd.eventNodes)
            {
                // Only connect this EventNode if a "next" is explicitly defined
                if (end.next_id != "")
                    tempContext.Connect(end.id, end.next_id);
            }
        }
    }

    public static void Complete(string context, string taskID) {
        if (context == "" || taskID == "") {
            Debug.Log("Not completing task, empty task or context");
            return;
        }
        EventContext.CONTEXTS[context].Complete(taskID);
    }

    /// <summary>
    /// Determine whether the given taskID is "present" within the given context
    /// </summary>
    /// <param name="context">Context within which to check</param>
    /// <param name="taskID">TaskID to check for</param>
    /// <returns></returns>
    public static bool CheckEventPresent(string context, string id) {
        EventContext c = EventContext.GetContext(context);
        foreach (EventContext.EventNode n in c.Present)
        {
            Debug.Log(n.ID + ", " + id);

            if (n.ID == id)
                return true;
        }
        return false;
    }

    private void Start() {
        //tutorial.Start("pointer");

        foreach (EventContextDefinition cd in SerializedEventContexts)
        {
            EventContext.GetContext(cd.id).Start(cd.eventNodes[0].id);
        }
    }
}
