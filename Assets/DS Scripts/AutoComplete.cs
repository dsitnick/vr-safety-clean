﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// An Encounter driver that automatically completes the Encounter that opened it.
/// </summary>
public class AutoComplete : MonoBehaviour {
    void OnEnable() {
        Encounter e = GetComponentInParent<Encounter>();
        if (e) {
            e.Complete();
        }
    }
}
