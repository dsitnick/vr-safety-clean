﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace MosaicVR {
    /// <summary>
    /// Used to manage events intended to be executed and checked in specific orders. Ideal for tutorial levels
    /// Initial Steps:
    /// Add() - Add all nodes and tasks
    /// Connect() - Connect the nodes together
    /// AddCompleteEvent() - Assign event listeners to completion
    /// 
    /// Run Steps:
    /// Start() - Begin the context, start tracking completed events
    /// Complete() - Complete events until done
    /// </summary>
    public class EventContext { 

        public static Dictionary<string, EventContext> CONTEXTS = new Dictionary<string, EventContext>();
        public static EventContext GetContext(string name) { return CONTEXTS[name]; }

        //These store the nodes completed, the nodes awaiting completion, and the nodes yet to be unlocked
        //At the start, future is full of all but one task (starting task) which is in present, past is empty
        //The final event being in past is the critera for the EventContext's completion
        private HashSet<EventNode> past, present, future;
        public HashSet<EventNode> Present { get { return present; } }

        //Stores the mapping of unique ids of nodes to each node class
        private Dictionary<string, EventNode> nodeMap;

        //Stores a mapping of ids to unity events invoked by completion of that node
        private Dictionary<string, UnityEvent> startMap, completeMap;

        //Cosmetic name for event context
        private string name;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="Event Context Name"></param>
        public EventContext(string name) {
            this.name = name;
            past = new HashSet<EventNode>();
            present = new HashSet<EventNode>();
            future = new HashSet<EventNode>();
            nodeMap = new Dictionary<string, EventNode>();
            startMap = new Dictionary<string, UnityEvent>();
            completeMap = new Dictionary<string, UnityEvent>();
            CONTEXTS.Add(name, this);
        }

        /// <summary>
        /// Adds a task node to the EventContext with a name and a variable number of task ids. 
        /// This should all be done before starting
        /// </summary>
        /// <param name="Unique Node Name"></param>
        /// <param name="Task IDs"></param>
        public void Add(string name, params string[] ids) {
            if (nodeMap.ContainsKey(name)) {
                Debug.LogError("Event Context " + name + " already contains a node with id: " + name);
                return;
            }
            EventNode node = new EventNode(name, ids);
            nodeMap.Add(name, node);
        }

        /// <summary>
        /// Connects two nodes in the context together based on ids.
        /// When the source node is complete, it will add the destination node to the set of completable nodes
        /// This is to be called before starting
        /// </summary>
        /// <param name="Source Node ID"></param>
        /// <param name="Destination Node ID"></param>
        public void Connect(string srcNode, string destNode) {
            if (!nodeMap.ContainsKey(srcNode)) {
                Debug.LogError("Event Context " + name + " can't find src " + srcNode);
                return;
            }
            if (!nodeMap.ContainsKey(destNode)) {
                Debug.LogError("Event Context " + name + " can't find dest " + destNode);
                return;
            }
            nodeMap[srcNode].Connect(nodeMap[destNode]);
        }

        /// <summary>
        /// Returns a Unity event to be invoked when the given nodeID is started
        /// If the event has already been accessed from nodeID, it just returns that event instance, otherwise it creates a new one
        /// </summary>
        /// <param name="Node ID"></param>
        /// <returns>Unity Event corresponding to nodeID</returns>
        public UnityEvent StartEvent(string nodeID) {
            if (!nodeMap.ContainsKey(nodeID)) {
                Debug.LogError("Event Context " + name + " can't add start to " + nodeID);
                return null;
            }
            if (startMap.ContainsKey(nodeID)) {
                return startMap[nodeID];
            }
            UnityEvent e = new UnityEvent();
            startMap.Add(nodeID, e);
            return e;
        }

        /// <summary>
        /// Returns a Unity event to be invoked when the given nodeID is completed
        /// If the event has already been accessed from nodeID, it just returns that event instance, otherwise it creates a new one
        /// </summary>
        /// <param name="Node ID"></param>
        /// <returns>Unity Event corresponding to nodeID</returns>
        public UnityEvent CompletionEvent(string nodeID) {
            if (!nodeMap.ContainsKey(nodeID)) {
                Debug.LogError("Event Context " + name + " can't add completion to " + nodeID);
                return null;
            }
            if (completeMap.ContainsKey(nodeID)) {
                return completeMap[nodeID];
            }
            UnityEvent e = new UnityEvent();
            completeMap.Add(nodeID, e);
            return e;
        }

        /// <summary>
        /// Links the completion of the given node to the completion of the given task
        /// </summary>
        /// <param name="Node ID"></param>
        /// <param name="Task ID"></param>
        public void AddCompleteTask(string nodeID, string taskID) {
            if (!nodeMap.ContainsKey(nodeID)) {
                Debug.LogError("Event Context " + name + " can't add completion task to " + nodeID);
                return;
            }
            nodeMap[nodeID].AddCompleteTask(taskID);
        }

        /// <summary>
        /// Starts the event context, starting at the given node ids. This allows for beginning at different points of the contexts
        /// </summary>
        /// <param name="Starting Node IDs"></param>
        public void Start(params string[] nodeIDs) {
            foreach (string i in nodeIDs) {
                if (!nodeMap.ContainsKey(i)) {
                    Debug.LogError("Event Context " + name + " can't start with id " + i);
                    return;
                }
            }

            //Resets runtime sets
            past.Clear();
            present.Clear();
            future.Clear();

            //Adds the keys in nodeIDs to present, and adds the rest to future
            HashSet<string> ids = new HashSet<string>(nodeIDs);
            foreach (string s in nodeMap.Keys) {
                if (ids.Contains(s)) {
                    present.Add(nodeMap[s]);
                    startMap[s].Invoke();
                } else {
                    future.Add(nodeMap[s]);
                }
            }
            CheckCurrent(); //Progresses if applicable
        }

        /// <summary>
        /// Completes a task with the given task ID. If multiple current nodes have the same task, it will mark completion on all of them
        /// </summary>
        /// <param name="Completed Task ID"></param>
        public void Complete(string taskID) {
            foreach (EventNode e in present) {
                e.Complete(taskID);
            }

            CheckCurrent(); //Progresses if applicable
        }

        /// <summary>
        /// Progresses, transfering completed nodes from present to past, and transfering child nodes from future to present
        /// This also will invoke all of the completion events on completed nodes
        /// </summary>
        private void CheckCurrent() {
            List<EventNode> toAdd = new List<EventNode>();
            List<EventNode> toRemove = new List<EventNode>();

            foreach (EventNode e in present) {
                if (e.IsComplete()) {
                    foreach (EventNode c in e.Children) {
                        if (!past.Contains(c) && !present.Contains(c)) {
                            toAdd.Add(c);
                        }
                    }
                    foreach (string s in e.CompletionTasks) {
                        Complete(s);
                    }
                    past.Add(e);
                    toRemove.Add(e);
                    completeMap[e.ID].Invoke();
                }
            }

            // Need to wait until done iterating to modify present
            foreach(EventNode e in toAdd) {
                present.Add(e);
                startMap[e.ID].Invoke();
            }
            foreach(EventNode e in toRemove) {
                present.Remove(e);
            }
        }

        /// <summary>
        /// A single node for a group of events
        /// </summary>
        public class EventNode {
            /// <summary>
            /// Gets all nodes to unlock upon completion
            /// </summary>
            public HashSet<EventNode> Children { get { return children; } }

            private HashSet<EventNode> children;
            private HashSet<string> criteria, completed, tasks;
            private string id;
            private UnityEvent completion;
            public UnityEvent CompletionEvent { get { return completion; } }
            public HashSet<string> CompletionTasks { get { return tasks; } }
            /// <summary>
            /// Unique identifier for this task
            /// </summary>
            public string ID { get { return id; } }

            /// <summary>
            /// Constructor, adds all ids to criteria to be completed
            /// </summary>
            /// <param name="Node ID"></param>
            /// <param name="Task IDs"></param>
            public EventNode(string id, params string[] ids) {
                this.id = id;
                children = new HashSet<EventNode>();
                criteria = new HashSet<string>();
                completed = new HashSet<string>();
                tasks = new HashSet<string>();
                foreach (string i in ids) {
                    criteria.Add(i);
                }
                completion = new UnityEvent();
            }

            /// <summary>
            /// Resets the node, marking all tasks as incomplete
            /// </summary>
            public void Reset() {
                foreach (string s in completed) {
                    criteria.Add(s);
                }
                completed.Clear();
            }

            /// <summary>
            /// Adds the destination node to the set of child nodes
            /// </summary>
            /// <param name="Destination Node"></param>
            public void Connect(EventNode dest) {
                children.Add(dest);
            }

            /// <summary>
            /// Completes a given taskID, marking it as complete in the node
            /// </summary>
            /// <param name="taskIDID"></param>
            /// <returns>True iff the task was found incomplete</returns>
            public bool Complete(string taskID) {
                if (!criteria.Contains(taskID)) {
                    return false;
                }
                completed.Add(taskID);
                criteria.Remove(taskID);
                return true;
            }

            public void AddCompleteTask(string taskID) {
                tasks.Add(taskID);
            }

            /// <summary>
            /// Returns whether all tasks are complete
            /// </summary>
            public bool IsComplete() { return criteria.Count == 0; }
        }
    }
}