﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MosaicVR {
    public class MinimapObject : MonoBehaviour {

        public float radius;
        public Vector3 position { get { return transform.position; } }
        public Vector3 direction { get { return transform.forward; } }
        public float angle { get { return transform.eulerAngles.y; } }

        public virtual void Enable() { }
        public virtual  void Disable() { }
        public virtual void Teleport() { }
    }
}

