﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MosaicVR {
    public class MinimapWaypoint : MonoBehaviour {

        //private Minimap map;
        private MinimapObject obj;
        private Image image;

        private void Awake() {
            image = GetComponent<Image>();
        }

        public void Initialize(Minimap map, MinimapObject obj) {
            //this.map = map;
            this.obj = obj;
        }

        public void Toggle() {
            if (obj != null)
                Minimap.SetWaypoint(obj);
        }

        public void SetPos(Vector2 pos) {
            image.rectTransform.anchoredPosition = pos;
        }
        public void SetAngle(float angle) {
            image.rectTransform.localEulerAngles = Vector3.back * angle;
        }
    }
}
