﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MosaicVR {
    public class Minimap : MonoBehaviour {

        [Tooltip("Create an image, line it up with the bounds of the minimap, and disable it. This is used to calculate icon positions")]
        public Image canvasBounds;

        [Tooltip("Icon image for player location; Not a prefab")]
        public MinimapWaypoint playerIcon;

        [Tooltip("Transform of the player, should use the head transform")]
        public Transform playerTransform;

        [Tooltip("Prefab to spawn in icons for waypoints")]
        public GameObject waypointIconPrefab;

        [Tooltip("Determines the center and size of the map in the real world. This should be as accurate to the map geometry as possible")]
        public Rect mapBounds;

        [Tooltip("This is placed on the position of the highlighted object every frame, use this for spotlights or something")]
        public GameObject highlightObject;

        private Dictionary<MinimapObject, MinimapWaypoint> waypointIcons;

        private ParabolicPointer pointer = null;

        private MinimapObject selected = null;

        private static Minimap SINGLETON = null;
        public static MinimapObject SELECTED { get { return SINGLETON.selected; } }

        private void Awake() {
            SINGLETON = this;
            waypointIcons = new Dictionary<MinimapObject, MinimapWaypoint>();
            foreach (MinimapObject obj in FindObjectsOfType<MinimapObject>()) {
                GameObject g = Instantiate(waypointIconPrefab, playerIcon.transform.parent);
                MinimapWaypoint w = g.GetComponent<MinimapWaypoint>();
                waypointIcons.Add(obj, w);
                w.Initialize(this, obj);
            }
            playerIcon.Initialize(this, null);
            pointer = FindObjectOfType<ParabolicPointer>();
        }

        private void Start() {
            //SetWaypoint(null);
        }

        private void Update() {
            SetPos(playerIcon, playerTransform);
            playerIcon.SetAngle(playerTransform.localEulerAngles.y);
            foreach (MinimapObject obj in waypointIcons.Keys) {
                SetPos(waypointIcons[obj], obj.transform);
            }

        }

        private void SetPos(MinimapWaypoint icon, Transform obj) {
            Vector2 p = new Vector2(obj.position.x, obj.position.z) - mapBounds.position;
            p.x *= canvasBounds.rectTransform.rect.width / mapBounds.width;
            p.y *= canvasBounds.rectTransform.rect.height / mapBounds.height;
            icon.SetPos(p);
        }

        public static void SetWaypoint(MinimapObject obj) {
            if (!SINGLETON)
                SINGLETON = FindObjectOfType<Minimap>();

            SINGLETON.highlightObject.SetActive(obj != null);
            if (obj != null) { SINGLETON.highlightObject.transform.position = obj.transform.position; }

            foreach (MinimapObject o in SINGLETON.waypointIcons.Keys) {
                if (o == obj) {
                    o.Enable();
                } else {
                    o.Disable();
                }
            }

            SINGLETON.selected = obj;
            if (SINGLETON.pointer != null) {
                if (obj != null) {
                    SINGLETON.pointer.SetSnap(obj);
                } else {
                    SINGLETON.pointer.ClearSnap();
                }
            }
        }

        public static void ClearWaypoint() {
            SetWaypoint(null);
        }
    }
}
