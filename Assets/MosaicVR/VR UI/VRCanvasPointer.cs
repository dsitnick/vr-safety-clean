﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NewtonVR;

namespace MosaicVR{
	[RequireComponent(typeof(NVRHand))]
	[RequireComponent(typeof(LineRenderer))]
	public class VRCanvasPointer : MonoBehaviour {
		private const float MAX_DISTANCE = 50;
		public enum PointerMode {Toggle, Hold};

		private LineRenderer line;
		private NVRHand hand;
		private bool active = false;
		VRCanvasInteractable lastInteractable = null;

		[Tooltip("NVR button used to toggle the laser")]
		public NVRButtons ToggleButton;
		[Tooltip("NVR button used to select and use elements")]
		public NVRButtons UseButton;
		[Tooltip("Whether the laser is toggled or held")]
		public PointerMode Mode;
		[Tooltip("Color of the laser")]
		public Color color;

        public bool ToggleEnabled;

		void Awake(){
			line = GetComponent<LineRenderer> ();
			hand = GetComponent<NVRHand> ();
			SetColor (color);
		}

		/// <summary>
		/// Handles inputs
		/// </summary>
		void Update(){
			if (Mode == PointerMode.Hold) {
				//Hold to point

				//Appropriately toggles the laser depending on the input
				if (hand.Inputs [ToggleButton].PressDown && ToggleEnabled) {
					Toggle (true);
				}else if (hand.Inputs [ToggleButton].PressUp && ToggleEnabled) {
					Toggle (false);
				}
			} else {
				//Toggle

				//Toggles the laser on button press
				if (hand.Inputs [ToggleButton].PressDown && ToggleEnabled) {
					Toggle ();
				}
			}

			//If there is a hovered interactable, calls its select and deselect functions if applicable
			if (lastInteractable != null) {
				if (hand.Inputs [UseButton].PressDown) {
					lastInteractable.SelectDown ();
				} else if (hand.Inputs [UseButton].PressUp) {
					lastInteractable.SelectUp ();
				}
			}
		}

		/// <summary>
		/// Handles raycasting and the laser
		/// </summary>
		void FixedUpdate(){
			line.enabled = active; //To ensure no weird laser positions instead of enabling on toggle

			if (active) {
				//Laser is on

				RaycastHit h;
				Vector3 end;

				//Raycasts to get hit information
				if (Physics.Raycast (transform.position, transform.forward, out h)) {
					end = h.point;

					//This section hits an interactable, then checks to see if the "current interactable" has changed
					//If it has changed, then it needs to call an Enter event on the new one, and the Exit event on the old one
					//If the old/new interactables are null then they will be ignored but the other Enter/Exit call will still be invoked
					VRCanvasInteractable i = h.transform.GetComponent<VRCanvasInteractable> ();
					if (i != lastInteractable) {
						if (i != null)
							i.HoverEnter ();

						if (lastInteractable != null)
							lastInteractable.HoverExit ();
						
					}
					lastInteractable = i;

				} else {
					end = transform.position + transform.forward * MAX_DISTANCE;
				}

				//Sets the positions of the line
				//The end point is either the hit point of the raycast or the max distance along the ray
				line.SetPositions(new Vector3[]{transform.position, end});
			}
		}

		/// <summary>
		/// Toggles the laser
		/// </summary>
		public void Toggle(){
			Toggle (!active);
		}

		/// <summary>
		/// Sets the laser's activity
		/// </summary>
		public void Toggle(bool active){
			this.active = active;
			if (!active) {
				lastInteractable = null;
			}
		}

		/// <summary>
		/// Sets the color of the laser and "color" just for reference
		/// </summary>
		public void SetColor(Color color){
			this.color = color;
			line.startColor = line.endColor = color;
		}
	}
}