﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NewtonVR;

namespace MosaicVR{
	/// <summary>
	/// Used to represent a single page of a canvas
	/// One page is shown at a time per-canvas
	/// Some canvases may have only one page
	/// </summary>
	public abstract class VRCanvasPage : MonoBehaviour {

		protected VRCanvasElement[] elements;

		protected virtual void Awake(){
			elements = GetComponentsInChildren<VRCanvasElement> (true);
		}
		
		public virtual void Initialize(VRCanvas canvas, NVRPlayer player){
			foreach (VRCanvasElement e in elements) {
				e.Initialize (canvas, this);
			}
		}

		public virtual void Toggle(bool active){
			gameObject.SetActive (active);
		}
	}
}
