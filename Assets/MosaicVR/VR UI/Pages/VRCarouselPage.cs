﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NewtonVR;

namespace MosaicVR{
	/// <summary>
	/// Carousel pages follow the player, displaying elements around their head
    /// Elements are spaced out along a cirlce around the head at a fixed distance
    /// Using the GrabButton allows the user to spin the gallery, which has its own drag
	/// </summary>

	[AddComponentMenu("VR Canvas/Carousel Page")]
	public class VRCarouselPage : VRCanvasPage {

        public enum Orientation { Horizontal, Vertical };

        private const float ANGLE_RANGE = 180, //The total range of degrees before images dissapear
        SHRINK_GAP = 1f / 12f, //The percent of angle before the image starts shrinking, reaching 0 at edge of range
        LERP = 0.3f; //The lerp amount for the transform of this canvas, used to apply smoothing

        [Tooltip("Number of degrees between the centers of two adjacent elements")]
        public float AngleGap;

        [Tooltip("The distance of the centers of all elements from the player")]
        public float Radius;
        
        [Tooltip("Multiplier called every Fixed frame on inertia, slowing it to a stop")]
        public float Drag;

        [Tooltip("Button used for spinning the gallery")]
        public NVRButtons GrabButton;

        public Orientation orientation;

        [Tooltip("Determine whether the carousel allows grabbing of elements")]
        public bool Grabbable;

        //Using these private members to ensure safety
        private float gap, radius, drag;
        private NVRButtons button;
        private Orientation or;
        private bool grabbable;

        private Transform head;
        private NVRHand leftHand, rightHand;

        private bool active, grabbing;
        private NVRHand scrollHand;
        private float angle, grabAngle, grabHandPos, lastAngle, inertia;

        protected override void Awake() {
            base.Awake();
            gap = AngleGap;
            radius = Radius;
            drag = Drag;
            button = GrabButton;
            or = orientation;
            grabbable = Grabbable;
        }

        public override void Initialize(VRCanvas canvas, NVRPlayer player) {
            if (player == null) {
                Debug.Log("Carousel page can't function without head and hands!");
                return;
            }
            base.Initialize(canvas, player);
            head = player.Head.transform;
            rightHand = player.RightHand;
            leftHand = player.LeftHand;
            angle = lastAngle = grabAngle = grabHandPos = inertia = 0;
            scrollHand = null;
            if (grabbable) {
                NVRInteractableItem[] items = GetComponentsInChildren<NVRInteractableItem>();
                foreach (NVRInteractableItem item in items) {
                    item.OnBeginInteraction.AddListener(delegate { Grab(true); });
                    item.OnEndInteraction.AddListener(delegate { Grab(false); });
                }
            }
        }

        public override void Toggle(bool active) {
            //this.active = active;
        }

        private void Grab(bool grabbing) {
            this.grabbing = grabbing;
        }

        private void FixedUpdate() {
            

            transform.position = Vector3.Lerp(transform.position, head.position, LERP);
            transform.rotation = Quaternion.Lerp(transform.rotation, head.rotation, LERP);

            if (grabbing)
                return;

            if (scrollHand != null) {
                //Scroll code here
                angle = grabAngle + CalcAngle() - grabHandPos;

                SetRotation(angle);
                inertia = angle - lastAngle;
                lastAngle = angle;

                //Put at end
                //While holding, this checks to release the hand
                if (!scrollHand.Inputs[button].IsPressed) {
                    scrollHand = null;
                }
            } else {
                angle += inertia;
                inertia *= drag;
                SetRotation(angle);

                //Put at end
                //While not holding, this checks to add a hand to holding
                if (rightHand.Inputs[button].PressDown) {
                    scrollHand = rightHand;
                    grabHandPos = CalcAngle();
                    grabAngle = angle;
                }
                if (leftHand.Inputs[button].PressDown) {
                    scrollHand = leftHand;
                    grabHandPos = CalcAngle();
                    grabAngle = angle;
                }
            }
        }

        void SetRotation(float angle) {
            //Iterates through each element, determining its own private angle and whether it should be displayed
            for (int i = 0; i < elements.Length; i++) {
                float a = i * gap + angle; //Angle of this specific element
                a %= gap * elements.Length;
                if (a < 0) {
                    a += gap * elements.Length;
                }
                if (a <= ANGLE_RANGE) {
                    //Enables element
                    //Sets its position along the circle centered on the head, with specified radius
                    //Sets its rotation to always point at the player
                    elements[i].Toggle(true);
                    if (or == Orientation.Horizontal) {
                        elements[i].transform.localPosition = (Vector3.right * Mathf.Cos(a * Mathf.Deg2Rad) + Vector3.forward * Mathf.Sin(a * Mathf.Deg2Rad)) * radius;
                        elements[i].transform.localEulerAngles = Vector3.up * (180 - a);
                    } else {
                        elements[i].transform.localPosition = (Vector3.up * Mathf.Cos(a * Mathf.Deg2Rad) + Vector3.forward * Mathf.Sin(a * Mathf.Deg2Rad)) * radius;
                        elements[i].transform.localEulerAngles = Vector3.right * a;
                    }
                    

                    //Sets the scale of the element, lerps from 0 to 1 on shrink gaps
                    if (a <= ANGLE_RANGE * SHRINK_GAP) {
                        //Left edge
                        elements[i].transform.localScale = Vector3.one * (a / (ANGLE_RANGE * SHRINK_GAP));
                    } else if (ANGLE_RANGE - a <= ANGLE_RANGE * SHRINK_GAP) {
                        //Right edge
                        elements[i].transform.localScale = Vector3.one * ((ANGLE_RANGE - a) / (ANGLE_RANGE * SHRINK_GAP));
                    } else {
                        //Center
                        elements[i].transform.localScale = Vector3.one;
                    }
                } else {
                    //Out of range, disable element
                    elements[i].Toggle(false);
                }
            }
        }

        //Returns the angle of the hand used to scroll
        private float CalcAngle() {
            if (scrollHand == null) {
                return 0;
            }
            Vector3 v = scrollHand.CurrentPosition - head.position;
            v.Normalize();
            float x, y;
            if (or == Orientation.Horizontal) {
                x = Vector3.Dot(v, head.right);
                y = Vector3.Dot(v, head.forward);
            } else {
                x = Vector3.Dot(v, head.up);
                y = Vector3.Dot(v, head.forward);
            }
            
            return Mathf.Atan2(y, x) * Mathf.Rad2Deg;
        }
    }
}
