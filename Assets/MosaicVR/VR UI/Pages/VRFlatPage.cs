﻿using System.Collections;
using System.Collections.Generic;
using NewtonVR;
using UnityEngine;

namespace MosaicVR {
    [AddComponentMenu("VR Canvas/Page")]
    public class VRFlatPage : VRCanvasPage {
        public override void Initialize(VRCanvas canvas, NVRPlayer player) {
            base.Initialize(canvas, player);
        }

        public override void Toggle(bool active) {
            base.Toggle(active);
        }
    }
}