﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NewtonVR;

namespace MosaicVR{
	/// <summary>
	/// Carousel pages follow the player, displaying elements around their head
    /// Elements are spaced out along a cirlce around the head at a fixed distance
    /// Using the GrabButton allows the user to spin the gallery, which has its own drag
	/// </summary>

	[AddComponentMenu("VR Canvas/Slide Page")]
	public class VRSlidePage : VRCanvasPage {

        public enum Orientation { Horizontal, Vertical };

        private const float LERP = 0.3f; //The lerp amount for the transform of this canvas, used to apply smoothing

        [Tooltip("Distance between the centers of two adjacent elements")]
        public float Gap;
        
        [Tooltip("Multiplier called every Fixed frame on inertia, slowing it to a stop")]
        public float Drag;

        [Tooltip("The width of display")]
        public float Width;

        [Tooltip ("The distance from the center when the edges start to shrink. 0 for no shrink")]
        public float ShrinkThreshold;

        [Tooltip("Button used for spinning the gallery")]
        public NVRButtons GrabButton;

        public Orientation orientation;

        [Tooltip("Determine whether the carousel allows grabbing of elements")]
        public bool Grabbable;

        private NVRPlayer player;

        //Using these private members to ensure safety
        private Orientation or;
        private bool grabbable;

        private NVRHand leftHand, rightHand;

        private bool active, grabbing;
        private NVRHand scrollHand;
        private float pos, grabPos, grabHandPos, lastPos, inertia;

        protected override void Awake() {
            base.Awake();
            or = orientation;
            grabbable = Grabbable;
            player = FindObjectOfType<NVRPlayer>();
        }

        private void Start() {
            Initialize(null, player);
        }

        public override void Initialize(VRCanvas canvas, NVRPlayer player) {
            if (player == null) {
                Debug.Log("Carousel page can't function without head and hands!");
                return;
            }
            base.Initialize(canvas, player);
            rightHand = player.RightHand;
            leftHand = player.LeftHand;
            pos = lastPos = grabPos = grabHandPos = inertia = 0;
            scrollHand = null;
            if (grabbable) {
                NVRInteractableItem[] items = GetComponentsInChildren<NVRInteractableItem>();
                foreach (NVRInteractableItem item in items) {
                    item.OnBeginInteraction.AddListener(delegate { Grab(true); });
                    item.OnEndInteraction.AddListener(delegate { Grab(false); });
                }
            }
        }

        public override void Toggle(bool active) {
            //this.active = active;
        }

        private void Grab(bool grabbing) {
            this.grabbing = grabbing;
        }

        private const float MAX_DISTANCE = 1;
        private void FixedUpdate() {
            if (grabbing)
                return;
            
            if (scrollHand != null && Vector3.Distance(scrollHand.CurrentPosition, transform.position) < MAX_DISTANCE) {
                //Scroll code here
                pos = grabPos + CalcPos () - grabHandPos;

                SetPosition(pos);
                inertia = pos - lastPos;
                lastPos = pos;

                //Put at end
                //While holding, this checks to release the hand
                if (!scrollHand.Inputs[GrabButton].IsPressed) {
                    scrollHand = null;
                }
            } else {
                pos += inertia;
                inertia *= Drag;
                SetPosition(pos);

                //Put at end
                //While not holding, this checks to add a hand to holding
                if (rightHand.Inputs[GrabButton].PressDown) {
                    scrollHand = rightHand;
                    grabHandPos = CalcPos();
                    grabPos = pos;
                }
                if (leftHand.Inputs[GrabButton].PressDown) {
                    scrollHand = leftHand;
                    grabHandPos = CalcPos ();
                    grabPos = pos;
                }
            }
        }

        void SetPosition(float pos) {
            //Iterates through each element, determining its own private angle and whether it should be displayed
            for (int i = 0; i < elements.Length; i++) {
                float x = i * Gap + pos; //Pos of this specific element
                x %= Gap * elements.Length;
                if (x < 0) {
                    x += Gap * elements.Length;
                }
                x -= Width / 2f;

                if (Mathf.Abs(x) <= Width / 2) {
                    //Enables element
                    elements[i].Toggle (true);
                    elements[i].transform.localPosition = (or == Orientation.Horizontal ? Vector3.right : Vector3.up) * (x);;

                    if (ShrinkThreshold > 0 && Width / 2f - Mathf.Abs(x) < ShrinkThreshold) {
                        //Edge
                        elements[i].transform.localScale = Vector3.one * (Width / 2f - Mathf.Abs (x)) / ShrinkThreshold;
                    } else {
                        //Center
                        elements[i].transform.localScale = Vector3.one;
                    }

                } else {
                    elements[i].Toggle (false);
                }
            }
        }

        //Returns the angle of the hand used to scroll
        private float CalcPos() {
            if (scrollHand == null) {
                return 0;
            }
            Vector3 v = scrollHand.CurrentPosition - transform.position;
            return or == Orientation.Horizontal ? Vector3.Dot(v, transform.right) : Vector3.Dot(v, transform.up);
        }
    }
}
