﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MosaicVR{
	/// <summary>
	/// The most basic element of a VR canvas
	/// These are extended to create buttons, sliders, images, 3D models, etc
	/// </summary>
	public abstract class VRCanvasElement : MonoBehaviour {

		protected VRCanvas canvas;
		protected VRCanvasPage page;

		//Called when page is created
		public void Initialize(VRCanvas canvas, VRCanvasPage page){
			this.canvas = canvas;
			this.page = page;
		}

        public abstract void Toggle(bool active);
	}
}