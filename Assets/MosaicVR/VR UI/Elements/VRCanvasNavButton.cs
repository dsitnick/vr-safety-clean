﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace MosaicVR{
	/// <summary>
	/// Subclass of buttons designed to simply navigate to the given page
	/// </summary>

	[AddComponentMenu("VR Canvas/Navigation Button")]
	public class VRCanvasNavButton : VRCanvasButton {

		[Space(10)]

		public GameObject navPage;

		public override void SelectDown () {
			if (navPage == null) {
				Debug.LogError ("You're trying to load a null page");
				return;
			}
			base.SelectDown ();
			canvas.LoadPage (navPage);
		}
	}

}