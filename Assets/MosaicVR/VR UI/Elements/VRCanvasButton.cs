﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MosaicVR{
	/// <summary>
	/// UNIMPLEMENTED: Should change colors and make sounds when pressed
	/// </summary>

	[AddComponentMenu("VR Canvas/Button")]
	public class VRCanvasButton : VRCanvasInteractable {
		
		public override void HoverEnter () {
			base.HoverEnter ();
		}

		public override void HoverExit () {
			base.HoverExit ();
		}

		public override void SelectDown () {
			base.SelectDown ();
		}

		public override void SelectUp () {
			base.SelectUp ();
		}

        public override void Toggle(bool active) {
            gameObject.SetActive(active);
        }
    }

}