﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace MosaicVR {
	/// <summary>
	/// Represents a canvas element which can be interacted with by a VRCanvasPointer
	/// Has events for HoverEnter, HoverExit, SelectDown, and SelectUp
	/// </summary>
	public abstract class VRCanvasInteractable : VRCanvasElement {
		public UnityEvent OnHoverEnter, OnHoverExit, OnSelectDown, OnSelectUp;

		protected bool isHover, isSelect;

		/// <summary>
		/// Activates when pointer enters this element
		/// Called from FixedUpdate
		/// </summary>
		public virtual void HoverEnter(){
			isHover = true;
			OnHoverEnter.Invoke ();
		}

		/// <summary>
		/// Activates when pointer exits this element
		/// Called from FixedUpdate
		/// </summary>
		public virtual void HoverExit(){
			isHover = false;
			OnHoverExit.Invoke ();

			if (isSelect)
				SelectUp ();

		}

		/// <summary>
		/// Activates when pointer selects this element
		/// Called from Update
		/// </summary>
		public virtual void SelectDown (){
			isSelect = true;
			//OnSelectDown.Invoke ();
		}

		/// <summary>
		/// Activates when pointer deselects this element
		/// Called from Update
		/// </summary>
		public virtual void SelectUp(){
			isSelect = false;
			OnSelectUp.Invoke ();
		}
	}
}