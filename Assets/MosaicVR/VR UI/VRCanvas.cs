﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NewtonVR;

namespace MosaicVR {
	/// <summary>
	/// Designed to manage a set of VRPages
	/// Uses prefabs for each page, and instantiates and destroys the newest page
	/// //Old pages in the history are kept disabled, which allows them to be quickly re-enabled on "back" events
	/// </summary>

	[AddComponentMenu("VR Canvas/Canvas")]
	public class VRCanvas : MonoBehaviour {
        public enum Type { Pointer, Navigate};

		[Tooltip("The first page prefab to load")]
		public GameObject InitialPage;

        [Tooltip("Reference to the VR Player object. This must be set to work")]
        public NVRPlayer player;

        public Type type;

		private Stack<VRCanvasPage> history;
		private VRCanvasPage currentPage;

		void Start(){
            if (player == null) {
                Debug.LogError("Player not set in canvas " + gameObject.name + ", you need to assign it for the canvas to work");
            }
			history = new Stack<VRCanvasPage> ();
			currentPage = null;
			LoadPage (InitialPage);
		}

		/// <summary>
		/// Loads the page from a prefab, adding it to the heirarchy
		/// </summary>
		public void LoadPage(GameObject pagePrefab){
			if (currentPage != null) {
				//Deactivates last page and stores in stack
				currentPage.Toggle (false);
				history.Push (currentPage);
			}

			//Creates new page, and initializes it
			GameObject g = (GameObject)Instantiate (pagePrefab, transform);
			VRCanvasPage page = g.GetComponent<VRCanvasPage> ();
			page.Initialize (this, player);
			g.transform.localPosition = Vector3.zero;
			g.transform.localRotation = Quaternion.identity;
			currentPage = page;
		}

		/// <summary>
		/// Navigates back one page, removing the newer page from the herirarchy
		/// </summary>
		public void Back(){
			if (history.Count <= 0)
				return; //At initial page

			//Destroys current page and opens up last page
			VRCanvasPage page = history.Pop ();
			page.Toggle (true);
			Destroy (currentPage);
			currentPage = page;
		}

		//Used to toggle the currently active page
		public void ToggleCurrent(bool active){
			if (currentPage != null)
				currentPage.Toggle (active);
			
		}
	}
}
